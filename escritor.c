//
// Created by Juan Bernardo Gómez Mendoza on 11/9/23.
//

#include <stdio.h>
#include <unistd.h>
#include "escritor.h"
#include "lector.h"

#define NUMERO_DE_VERSIONES 25

void* tareaEscritor(void* param) {
    Libro_t *libro = param;
    for (int i=0; i<NUMERO_DE_VERSIONES; ++i) {
        // El escritor solo usa el mutex, ya que la lectura es mutuamente
        // excluyente con la escritura.
        pthread_mutex_lock(&(libro->mutex));
        sprintf(libro->mensaje,
                "\x1B[35mEsta es la versión %d del libro. \x1B[0m",
                i);
        printf("\n\r-> El escritor ha actualizado el libro. %s\n\r\n\r",
               libro->mensaje);
        usleep(100000); // 100ms
        pthread_mutex_unlock(&(libro->mutex));
        usleep(500000); // 500ms
    }
}
