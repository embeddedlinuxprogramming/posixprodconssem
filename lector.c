//
// Created by Juan Bernardo Gómez Mendoza on 11/9/23.
//

#include <stdio.h>
#include "lector.h"
#include <unistd.h>

void* tareaLector(void* param) {
    Libro_t* libro = param;   // El lector recibe un libro.
    static int numLectores = 0;
    numLectores++;  // Se incrementa con cada nuevo hilo lector.
    int curLector = numLectores;
    while (1) {
        int actual;
        sem_wait(&(libro->semaforo));
        sem_getvalue(&(libro->semaforo), &actual);
        // Si es el primer lector, bloquea el mutex para que no
        // escriban.
        if (actual == MAX_LECTORES_SIMULTANEOS-1) {
            pthread_mutex_lock(&(libro->mutex));
            printf("\x1B[31m-> El lector %d acaba de bloquear el "
                   "acceso a libro. \x1B[0m\n\r", curLector);
        }
        printf("\x1B[32mUn lector (%d) está leyendo el libro. "
               "Mensaje: %s \x1B[0m\n\r", curLector, libro->mensaje);
        usleep(100000);  // 100ms
        sem_post(&(libro->semaforo));
        sem_getvalue(&(libro->semaforo), &actual);
        // Si ya no quedan lectores, desbloquea el libro.
        if (actual == MAX_LECTORES_SIMULTANEOS) {
            pthread_mutex_unlock(&(libro->mutex));
            printf("\x1B[31m-> El lector %d acaba de desbloquear el "
                   "acceso a libro. \x1B[0m\n\r", curLector);
        }
        usleep(400000);  // 400ms
    }
}
