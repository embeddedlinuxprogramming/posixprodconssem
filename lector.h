//
// Created by Juan Bernardo Gómez Mendoza on 11/9/23.
//

#ifndef POSIXPRODCONSSEM_LECTOR_H
#define POSIXPRODCONSSEM_LECTOR_H

#include <pthread.h>
#include "libro.h"

typedef struct {
    pthread_t* lector;
    int numLectores;
    Libro_t* libro;
} ListaLectores_t;

void* tareaLector(void*);

#endif //POSIXPRODCONSSEM_LECTOR_H
