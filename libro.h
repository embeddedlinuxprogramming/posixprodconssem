//
// Created by jbgomezm on 11/9/23.
//

#ifndef POSIXPRODCONSSEM_LIBRO_H
#define POSIXPRODCONSSEM_LIBRO_H

#define NUM_LECTORES 5
#define MAX_LECTORES_SIMULTANEOS 3

#include <pthread.h>
#include <semaphore.h>

typedef struct {
    char mensaje[80];
    pthread_mutex_t mutex; // Para control de escritura.
    sem_t semaforo; // Para control de lectura.
} Libro_t;

#endif //POSIXPRODCONSSEM_LIBRO_H
