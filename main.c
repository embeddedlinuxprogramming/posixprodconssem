//
// Created by Juan Bernardo Gómez Mendoza on 11/9/23.
//

#include <pthread.h>
#include <malloc.h>
#include "escritor.h"
#include "lector.h"

int main(int argc, char* argv[]) {
    Libro_t libro;
    pthread_mutex_init(&(libro.mutex), NULL);
    sem_init(&(libro.semaforo), 0,
                MAX_LECTORES_SIMULTANEOS);
    pthread_t escritor;
    pthread_t lectores[NUM_LECTORES];
    pthread_create(&escritor, NULL,
                   tareaEscritor, &libro);
    // Creación de los hilos.
    for (size_t desp=0; desp<NUM_LECTORES; ++desp)
        pthread_create(lectores + desp, NULL,
                       tareaLector, &libro);
    // Espera a finalizar la ejecución de los hilos.
    for (int i=0; i<NUM_LECTORES; ++i)
        pthread_join(lectores[i], NULL);
    pthread_join(escritor, NULL);
    // Destrucción del mutex y el semáforo.
    pthread_mutex_destroy(&(libro.mutex));
    sem_destroy(&(libro.semaforo));
    return 0;
}

